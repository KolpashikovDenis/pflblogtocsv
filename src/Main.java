import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {
    static List<String> lines;
//    static final String regExp1 = "(?<=\\:\\s)(.*?)(?=\\t)";
//    static final String regExp2 = "";
//    static final String regExp3 = "";
//    static final String regExp4 = "";

    public static void main(String[] args) {
        if(args.length < 3){
            System.out.println("Usage: <delimiter> <log_file> <new_file>");
        } else{
            try {
                lines = Files.readAllLines(Paths.get(args[1]), StandardCharsets.ISO_8859_1);
                File fOut = new File(args[2]+".csv");
                fOut.createNewFile();
                fOut.setWritable(true);
                OutputStream fOutput = new FileOutputStream(fOut);
                OutputStreamWriter fWriter = new OutputStreamWriter(fOutput, "ISO-8859-1");

                String line;
                StringBuilder sb = new StringBuilder();
                String t;
                for(int i = 0; i < lines.size(); i++){
                    line = lines.get(i);
                    sb.append(line.substring(0, 10));
                    sb.append(args[0]);
                    sb.append(line.substring(11, 23));
                    sb.append(args[0]);
                    t = line.substring(24, 30);
                    sb.append(t);
                    sb.append(args[0]);
                    if(t.contains("INFO")){
                        sb.append(line.substring(32, line.length()-1));
                        sb.append(args[0]);
                    } else{
                        int n = line.indexOf("\t");
                        sb.append(line.substring(31,n));
                        sb.append(args[0]);
                        sb.append(line.substring(n, line.length()-1));
                        sb.append(args[0]);
                    }
                    sb.append("\r\n");
                    fWriter.write(sb.toString());
                    fWriter.flush();
                    sb.setLength(0);
                }
                System.out.println("Done.");
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
